package it.unibo.oop.lab.nesting2;
import java.util.*;

/**
 * Represent a finite sequence of elements
 * @param <T>
 *            specific type
*/

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private final List<T> list;
	
	/**
	 * 
	 * Builds a new {@link OneListAcceptable}.
	 * @param list
	 * 				list of elements
	*/
	
	public OneListAcceptable(List<T> list) {
		this.list= Collections.unmodifiableList(list);
	}
	/**
	 * @return
	 * 			a new {@link AcceptorImpl}
	 * */
	public Acceptor<T> acceptor() {
		/**
		 * Represent an inspector of elements
		 * */
		class AcceptorImpl implements Acceptor<T>{
			
			private final Set<T> defenseList = new HashSet<>();
			private int counter=0;
			
			/**
			 * Verify if the 'newElement' is contained in the list
			 * 
			 * @param newElement
			 * 					The element that must be inspected at the list
			 * 
			 * @throws 
			 * 			ElementNotAcceptedException if the 'newElement' isn't contained at the list
			 * */
			public void accept(final T newElement) throws ElementNotAcceptedException {
				if(!OneListAcceptable.this.list.contains(newElement)) {
					throw new ElementNotAcceptedException(newElement);
				}
				if(!defenseList.contains(newElement)) {
					defenseList.add(newElement);
					counter++;
				}
			}
			
			/**
			 * Verify if {@link AcceptorImpl} inspected all the elements
			 * 
			 * @throws EndNotAcceptedException
			 * 					if {@link AcceptorImpl} didn't inspect all the elements
			 * */
			
			public void end() throws EndNotAcceptedException {
				if(!(OneListAcceptable.this.list.size() == counter)) {
					throw new EndNotAcceptedException();
				}
			}
		}
		return new AcceptorImpl();
	}
}