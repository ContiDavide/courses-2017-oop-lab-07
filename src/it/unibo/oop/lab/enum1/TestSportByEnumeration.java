package it.unibo.oop.lab.enum1;

import java.util.*;
import static it.unibo.oop.lab.enum1.Sport.*;


import it.unibo.oop.lab.socialnetwork.SocialNetworkUser;
import it.unibo.oop.lab.socialnetwork.SocialNetworkUserImpl;
import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */


public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    public static boolean checkUserOrder(final List<User> expected, final List<User> result) {
        for (int i = 0; i < expected.size(); i++) {
            if (!expected.get(i).equals(result.get(i))) {
                System.out.println("[EXCEPTION] [POS. " + i + "] [EXPECTED] " + expected.get(i) + " [GOT] " + result.get(i));
                return false;
            }
        }
        return true;
    }
    
    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	/*
         * create 6 social network users
         */
        final SportSocialNetworkUserImpl<User> kbacon = new SportSocialNetworkUserImpl<>("Kevin", "Bacon", "kbacon", 56);
        final SportSocialNetworkUserImpl<User> dwashington = new SportSocialNetworkUserImpl<>("Denzel", "Washington", "dwashington", 59);
        final SportSocialNetworkUserImpl<User> mgladwell = new SportSocialNetworkUserImpl<>("Malcom", "Gladwell", "mgladwell", 51);
        final SportSocialNetworkUserImpl<User> ntaleb = new SportSocialNetworkUserImpl<>("Nicholas", "Taleb", "ntaleb", 54);
        final SportSocialNetworkUserImpl<User> mrossi = new SportSocialNetworkUserImpl<>("Mario", "Rossi", "mrossi", 31);
        final SportSocialNetworkUserImpl<User> pverdi = new SportSocialNetworkUserImpl<>("Paolo", "Verdi", "pverdi", 24);
        // TEST on DENZEL
        dwashington.addFollowedUser("relatives", mrossi);
        dwashington.addFollowedUser("relatives", pverdi);
        dwashington.addFollowedUser("colleagues", kbacon);
        dwashington.addFollowedUser("writers", mgladwell);
        dwashington.addFollowedUser("writers", ntaleb);
        
        dwashington.addSport(F1);
        kbacon.addSport(MOTOGP);
        
        System.out.println(dwashington.hasSport(F1));
        System.out.println(kbacon.hasSport(MOTOGP));
        
        final List<User> denzelUsers = dwashington.getFollowedUsers();
        /*
         * Order denzel's followed users incrementally by age:
         * 
         * - define an anonymous comparator to sort incrementally by age
         * 
         * NOTE: in order to sort a list think about a method of the utility
         * class java.util.Collections
         * 
         * REFER TO LESSON 12-Advanced-Mechanisms.pdf, slide 39
         */    
        Collections.sort(denzelUsers, new Comparator<User>(){
			public int compare(User a, User b) {
				return a.getAge() - b.getAge();
			}
        });
        /*
         * expected Result
         */
        List<User> expectedResult = new ArrayList<User>();
        expectedResult.add(pverdi);
        expectedResult.add(mrossi);
        expectedResult.add(mgladwell);
        expectedResult.add(ntaleb);
        expectedResult.add(kbacon);
        System.out.println("[Order by age (increasing) Denzel friends] [TEST] [START]");
        System.out.println("[Order by age (increasing) Denzel friends] [TEST] [RESULT] "
                + checkUserOrder(expectedResult, denzelUsers));
        System.out.println("[Order by age (increasing) Denzel friends] [TEST] [END]");
        /*
         * TEST on MARIO ROSSI
         */
       
        mrossi.addFollowedUser("relatives", pverdi);
        mrossi.addFollowedUser("actors i like", kbacon);
        mrossi.addFollowedUser("science writers", mgladwell);
        mrossi.addFollowedUser("economists", ntaleb);
        mrossi.addFollowedUser("actors i like", dwashington);
        final List<User> rossiUsers = mrossi.getFollowedUsers();
        /*
         * Order rossi's followed users by age in decreasing order:
         * 
         * - define an anonymous comparator to sort by decrementing age
         * 
         * NOTE: in order to sort a list think about a method of the utility
         * class Collections
         */
        
        Collections.sort(rossiUsers, new Comparator<User>(){
			public int compare(User a, User b) {
				return b.getAge() - a.getAge();
			}
        });
        /*
         * expected Result
         */
               
        expectedResult = new ArrayList<User>();
        expectedResult.add(dwashington);
        expectedResult.add(kbacon);
        expectedResult.add(ntaleb);
        expectedResult.add(mgladwell);
        expectedResult.add(pverdi);
        System.out.println("[Order by age (decreasing) Rossi friends] [TEST] [START]");
        System.out.println("[Order by age (decreasing) Rossi friends] [TEST] [RESULT] "
                + checkUserOrder(expectedResult, rossiUsers));
        System.out.println("[Order by age (decreasing) Rossi friends] [TEST] [END]");
    }    	
}